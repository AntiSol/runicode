/*
Runicode - Javascript version.

(C)opyright Dale Magee, 2020. BSD 3-clause license.

This code:

* injects a style tag into the document to load the Junicode font, to enable display of runes.

* Traverses the DOM, converting latin characters into runes (Tolkien's Hobbit Runes)

*/

/*
	Map of english (latin) characters to runes.
	index may be:
		a single character
		two characters
		a single unicode value 

	value may be the unicode value of the rune, 
		or an array of values for multiple runes
*/
console.log("Ye Olde Runicode Bookmarklet started!");
var inchars = 0, outchars = 0, skipped = 0, started = Date.now();
var map = {
	' ': [0x0020,0x16eb,0x0020], /* spaces, so that word-wrapping doesn't break */
	'a': 0x16ab, 'b': 0x16d2, 'c': 0x16b3, 'd': 0x16de, 'e': 0x16d6, 'ea': 0x16e0,
	'ee': 0x16df,'f': 0x16a0, 'g': 0x16b7, 'h': 0x16bb, 'i': 0x16c1, 'j': 0x16c1,
	'k': 0x16F1, 'l': 0x16da, 'm': 0x16d7, 'n': 0x16be, 'ng': 0x16DD,'o': 0x16a9,
	'oo': 0x16F3,'p': 0x16c8, 'q': [0x16b3,0x16b9],
	'r': 0x16b1, 's': 0x16cb, 'sh': 0x16F2,'st': 0x16E5,'t': 0x16cf, 'th': 0x16A6,
	'u': 0x16a2, 'v': 0x16a2, 'w': 0x16B9, 'x': 0x16c9, 'y': 0x16a3, 'z': 0x16e3,
	
	//Latin-1 Supplement:
	//0x00e0: [0x16ab,0x0300], //rune with combining grave mark!
	'à': [0x16ab,0x0300], 'á': [0x16ab,0x0301], 'â': [0x16ab,0x0302],
	'ã': [0x16ab,0x0303], 'ä': [0x16ab,0x0308], 'å': [0x16ab,0x030a],
	'æ': [0x16ab,0x16d6], 'ç': [0x16b3,0x0327], 'è': [0x16d6,0x0300],
	'é': [0x16d6,0x0301], 'ê': [0x16d6,0x0302], 'ë': [0x16d6,0x0308],
	'ì': [0x16c1,0x0300], 'í': [0x16c1,0x0301], 'î': [0x16c1,0x0302],
	'ï': [0x16c1,0x0308], 'ñ': [0x16be,0x0303], 'ò': [0x16a9,0x0300],
	'ó': [0x16a9,0x0301], 'ô': [0x16a9,0x0302], 'õ': [0x16a9,0x0303],
	'ö': [0x16a9,0x0308],
	//'ø':	//can't seem to find a diagonal stroke. 
	'ù': [0x16a2,0x0300], 'ú': [0x16a2,0x0301], 'û': [0x16a2,0x0302],
	'ü': [0x16a2,0x0308], 'ý': [0x16a3,0x0301], 'ÿ': [0x16a3,0x0308],
}
//console.log("map:",map);
function traverse(el){
	/*
	 * credit: https://stackoverflow.com/questions/10730309/find-all-text-nodes-in-html-page
	 */
	
	//do not convert text inside these tags to runes.
	//	these should be lowercase:
	ignoreTags=["style","script"];

	//attributes which should be runified (on any element):
	attrs=['value','title','alt','placeholder'];
	
	var n, walk=document.createTreeWalker(el,NodeFilter.SHOW_TEXT | NodeFilter.SHOW_ELEMENT,null,false),eles=0;
	//var n, walk=document.createTreeWalker(el,NodeFilter.SHOW_TEXT,null,false);
 	while(n=walk.nextNode()) {
		eles++;
		//console.log(eles,n);
		
		if (n.nodeName == "#text") {
			//don't double-up (broken!):
			//if (n.parentElement.className.indexOf('runic') >= 0) continue;
			
			//ignoreTags:
			if (ignoreTags.indexOf(n.parentElement.tagName.toLowerCase()) != -1) continue;
			
			//replace text in text nodes
			text = n.textContent;
			if (!text.trim()) continue; //nothing to do
			newtext = runify(text);
			if (newtext != text) {
				n.textContent = newtext
				cls = n.parentElement.className;
				if (cls) cls += " ";
				cls += "runic";
				n.parentElement.className = cls
			}
		} else {
			//runify attributes:
			for (ai=0;ai<attrs.length;ai++)
				if (n.hasAttribute(attrs[ai]) && (ot = n.getAttribute(attrs[ai]).trim())) {
					nt = runify(ot); //runify(n.getAttribute(attrs[i]).trim())
					if (nt != ot) 
						n.setAttribute(attrs[ai],nt);
				}
			
		}
 	}
}

function runify(text) {
	ret = "";
	for (i=0;i< text.length; i++) {
		//try 2 chars first:
		ch = text.substr(i,2).toLowerCase();
		if (ch in map) {	//2 chars
			i++;
			inchars++;
		} else 
			ch = text.substr(i,1);	//1 char
		lch = ch.toLowerCase();
		if (lch == " ") {
			//space is a special case, condense multiple whitespace to one
			re = /^[\s᛫]+/
			chk = text.substr(i);
			if (matches=re.exec(chk)) {
				if (matches[0].length>1) {
					i+= matches[0].length-1;
				}
			}
		}
		//to handle chars by unicode value:
		code = lch.charCodeAt(0);
		if (lch in map || code in map ) {
			inchars++;
			if (code in map) {
				v = map[code];
				console.log("code: ",code,"v:",v);
			} else 
				v = map[lch];
			
			if(Array.isArray(v)) {	//multiple symbols
				for (ix=0;ix<v.length;ix++) {
					ret += String.fromCharCode(v[ix]);
					outchars++;
				}
			} else {
				ret += String.fromCharCode(v);
				outchars++;
			}
		} else { //passthrough chars which have no runic equivalent:
			ret += ch
			skipped++;
		}
	}
	return ret;
}

head = document.head || document.getElementsByTagName('head')[0];
body = document.body || document.getElementsByTagName('body')[0];

//load the web font:
css = document.createElement('style');
css.type = "text/css";
css.appendChild(document.createTextNode('@font-face { font-family: "Junicode"; src: url("https://antisol.org/Junicode.woff") format("woff"); } html,.runic { font-family: Junicode; }'));;
head.appendChild(css);

//hack existing font-family styles so that they fall back to junicode
/* This doesn't work, security something something...

styles=document.styleSheets
for(i = 0; i<styles.length; i++) {
	for (ri=0; ri< styles[i].rules.length; ri++) { //BROKEN: can't access .rules across origins.
													//	we're probably going to have to do computed style for every element
													//	to add junicode. ugh.
		if (!(styles[i].rules[ri] instanceof CSSStyleRule)) continue;
		if (styles[i].rules[ri].style.fontFamily) {
			ff = styles[i].rules[ri].style.fontFamily;
			if (ff.trim() && (ff.indexOf('Junicode') == -1)) {
				ns = ff + ",Junicode";
				styles[i].rules[ri].style.fontFamily = ns;
			}
		}
	}
}
*/

//convert to runes:
traverse(body);
took=Date.now() - started;
console.log("Runification complete. Took " + took + " ms, " + inchars + " characters in, " + outchars + " runes out, " + skipped + " skipped. ");

